﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Models;

namespace WebApplication.AbstractServices
{
    public interface AbstractRepository<T> where T : AbstractEntity
    {
        IQueryable Show();
        T Find(string id);
        void Commit();
        void Update(T t);
        void Insert(T t);
        //void Delete(T AbstractEntity);
        void Delete(T t);
        bool Exist(string id);
    }
}

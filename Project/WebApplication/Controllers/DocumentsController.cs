﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication.AbstractServices;
using WebApplication.Data;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class DocumentsController : Controller
    {
        AbstractRepository<Document> _context;
       
        public DocumentsController(AbstractRepository<Document> context)
        {
            _context = context;
        }

        // GET: Documents
        public IActionResult Index()
        {
            return View(_context.Show());
        }

        // GET: Documents/Details/5
        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = _context.Find(id);
               
            if (document == null)
            {
                return NotFound();
            }

            return View(document);
        }

        // GET: Documents/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Documents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Title,DateDescription,id,CreationDate")] Document document)
        {
            if (ModelState.IsValid)
            {
                _context.Insert(document);
 
                return RedirectToAction(nameof(Index));
            }
            return View(document);
        }

        // GET: Documents/Edit/5
        public IActionResult Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document =  _context.Find(id);
            if (document == null)
            {
                return NotFound();
            }
            return View(document);
        }

        // POST: Documents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, [Bind("Title,DateDescription,id,CreationDate")] Document document)
        {
            if (id != document.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(document);
                 
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DocumentExists(document.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(document);
        }

        // GET: Documents/Delete/5
        public IActionResult Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = _context.Find(id);
               
            if (document == null)
            {
                return NotFound();
            }

            return View(document);
        }

        // POST: Documents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(string id)
        {
            var document = _context.Find(id);
            _context.Delete(document);
            return RedirectToAction(nameof(Index));
        }

        private bool DocumentExists(string id)
        {
            return _context.Exist(id);
        }
    }
}

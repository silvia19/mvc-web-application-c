﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication.AbstractServices;
using WebApplication.Data;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class MeetingsController : Controller
    {
        AbstractRepository<Meeting> _context;

        public MeetingsController(AbstractRepository<Meeting> context)
        {
            _context = context;
        }

        // GET: Meetings
        public  IActionResult Index()
        {
            return View(_context.Show());
        }

        // GET: Meetings/Details/5
        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meeting = _context.Find(id);
               
            if (meeting == null)
            {
                return NotFound();
            }

            return View(meeting);
        }

        // GET: Meetings/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Meetings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("FirstName,LastName,id,CreationDate")] Meeting meeting)
        {
            if (ModelState.IsValid)
            {
                _context.Insert(meeting);
                return RedirectToAction(nameof(Index));
            }
            return View(meeting);
        }

        // GET: Meetings/Edit/5
        public IActionResult Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meeting = _context.Find(id);
            if (meeting == null)
            {
                return NotFound();
            }
            return View(meeting);
        }

        // POST: Meetings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, [Bind("FirstName,LastName,id,CreationDate")] Meeting meeting)
        {
            if (id != meeting.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(meeting);
                  
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MeetingExists(meeting.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(meeting);
        }

        // GET: Meetings/Delete/5
        public IActionResult Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meeting = _context.Find(id);
               
            if (meeting == null)
            {
                return NotFound();
            }

            return View(meeting);
        }

        // POST: Meetings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(string id)
        {
            var meeting =  _context.Find(id);
            _context.Delete(meeting);
        
            return RedirectToAction(nameof(Index));
        }

        private bool MeetingExists(string id)
        {
            return _context.Exist(id);
        }
    }
}

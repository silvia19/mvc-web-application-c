﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.AbstractServices;

namespace WebApplication.Models
{
    public class User : AbstractEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public void Login() { }
        public void Request() { }
        public User() { }

       
        //public ICollection<Meeting> Meetings { get; set; }
        //public ICollection<Request> Requests { get; set; }
        public virtual List<Request> Requests { get; set; }
        public virtual List<Meeting> Meetings { get; set; }
        public virtual List<Message> Messages { get; set; }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class Document : AbstractEntity
    {
        public string Title { get; set; }
        public DateTime DateDescription { get; set; }

        //public virtual List<Request> Requests { get; set; }

    }
}

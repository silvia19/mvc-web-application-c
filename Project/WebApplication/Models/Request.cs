﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class Request : AbstractEntity
    {
        //public int UserId { get; set; }
        //public int EmployeeId { get; set; }
        public string Type { get; set; }
        public DateTime TimeRequest { get; set; }
        public Document Document { get; set; }

        public virtual List<Document> Documents { get; set; }
    }
}

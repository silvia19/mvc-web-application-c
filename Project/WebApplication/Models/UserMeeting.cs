﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class UserMeeting : AbstractEntity
    {
        public int UserIdNew { get; set; }
        public int MeetingIdNew { get; set; }
        public User User { get; set; }
        public Meeting Meeting { get; set; }
    }
}

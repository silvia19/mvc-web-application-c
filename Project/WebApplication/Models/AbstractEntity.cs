﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public abstract class AbstractEntity
    {
        public string id { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public AbstractEntity()
        {
            this.id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
    }
}

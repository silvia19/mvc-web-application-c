﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class Message : AbstractEntity
    {
        //public int UserId { get; set; }
        public DateTime DateDescriptionMessage { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}

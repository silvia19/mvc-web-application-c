﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class DocumentRequest : AbstractEntity
    {
        public int DocumentIdNew { get; set; }
        public int RequestIdNew { get; set; }
        public Document Document { get; set; }
        public Request Request { get; set; }

        //public virtual List<Request> Requests { get; set; }
        //public virtual List<Document> Documents { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.AbstractServices;
using WebApplication.Data;
using WebApplication.Models;

namespace WebApplication.ConcreteRepository
{
    public class ConcreteRepository<T> : AbstractRepository<T> where T : AbstractEntity
    {
        internal ApplicationDbContext _context;
        internal DbSet<T> dbSet;
        public ConcreteRepository(ApplicationDbContext context)
        {
            this._context = context;
            this.dbSet = context.Set<T>();
        }
        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Delete(T t)
        {
            dbSet.Remove(t);
            this.Commit();
        }

        public bool Exist(string id)
        {
            //return dbSet.Any<AbstractEntity>();
            return dbSet.Any((e => e.id == id));
        }

        public T Find(string id)
        {
            return dbSet.Find(id);
        }

        public void Insert(T t)
        {
            dbSet.Add(t);
            this.Commit();
        }

        public IQueryable Show()
        {
            return dbSet;
        }

        public void Update(T t)
        {
            dbSet.Update(t);
            this.Commit();
        }
    }
}
